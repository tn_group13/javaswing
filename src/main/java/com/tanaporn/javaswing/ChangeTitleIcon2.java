/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.javaswing;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author HP
 */
public class ChangeTitleIcon2 { // Example in Java Swing

    ChangeTitleIcon2() {
        JFrame f = new JFrame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\icon.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(200, 200);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new ChangeTitleIcon2();
    }
}
