/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.javaswing;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class JavaJOptionPane3 { //Example: showInputDialog()

    JFrame f;

    JavaJOptionPane3() {
        f = new JFrame();
        String name = JOptionPane.showInputDialog(f, "Enter Name");
    }

    public static void main(String[] args) {
        new JavaJOptionPane3();
    }
}
